FROM golang:latest

RUN go version
ENV GOPATH=/

COPY . .
RUN go mod download
RUN go build -o generator ./cmd/main.go

CMD ["./generator"]