package api

import (
	"generator-currency-pair-price/internals/app/handlers"
	"github.com/gorilla/mux"
	httpSwagger "github.com/swaggo/http-swagger"
	"net/http"
)

func CreateRoutes(currencyPairHandler *handlers.CurrencyPairHandler) *mux.Router {
	r := mux.NewRouter()

	r.HandleFunc("/currency_pair/list", currencyPairHandler.GetCurrencyPairList).Methods(http.MethodGet)
	r.HandleFunc("/currency_pair/find/{id}", currencyPairHandler.GetPricesByCurrencyPair).Methods(http.MethodGet)
	r.PathPrefix("/swagger/").Handler(httpSwagger.WrapHandler).Methods(http.MethodGet)

	r.NotFoundHandler = r.NewRoute().HandlerFunc(handlers.NotFound).GetHandler()
	return r
}
