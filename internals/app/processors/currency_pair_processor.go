package processors

import (
	"generator-currency-pair-price/internals/app/models"
	"generator-currency-pair-price/internals/storage"
)

type CurrencyPairInterface interface {
	GetPricesByCurrencyPair() models.CurrencyPair
	GetCurrencyPairList() []models.CurrencyPair
}

type CurrencyPairProcessor struct {
	storage storage.CurrencyPairStorageInterface
}

func NewCurrencyPairProcessor(storage storage.CurrencyPairStorageInterface) *CurrencyPairProcessor {
	processor := new(CurrencyPairProcessor)
	processor.storage = storage
	return processor
}

func (processor *CurrencyPairProcessor) GetPricesByCurrencyPair(currencyPair string) models.CurrencyPair {
	return processor.storage.GetPricesByCurrencyPair(currencyPair)
}

func (processor *CurrencyPairProcessor) GetCurrencyPairList() []models.CurrencyPair {
	return processor.storage.GetCurrencyPairList()
}
