package app

import (
	"context"
	"generator-currency-pair-price/internals/cfg"
	"net/http"
)

type Server struct {
	config cfg.Cfg
	srv    *http.Server
}

func NewServer(config cfg.Cfg) *Server {
	server := new(Server)
	server.config = config
	return server
}

func (server *Server) Run(routes http.Handler) error {
	server.srv = &http.Server{
		Addr:    ":" + server.config.Port,
		Handler: routes,
	}

	return server.srv.ListenAndServe()
}

func (server *Server) Shutdown(ctx context.Context) error {
	return server.srv.Shutdown(ctx)
}
