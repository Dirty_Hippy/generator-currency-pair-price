package handlers

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"
)

func WrapError(w http.ResponseWriter, err error) {
	WrapErrorWithStatus(w, err, http.StatusBadRequest)
}

func WrapErrorWithStatus(w http.ResponseWriter, err error, httpStatus int) {
	var response = Response{Result: "error", Data: err.Error()}
	res, err := json.Marshal(response)
	if err != nil {
		log.Fatalln(err)
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.Header().Set("X-Content-Type-Options", "nosniff")
	w.WriteHeader(httpStatus)
	fmt.Fprintln(w, string(res))
}

func WrapOK(w http.ResponseWriter, response Response) {
	res, err := json.Marshal(response)
	if err != nil {
		WrapErrorWithStatus(w, errors.New("marshal error"), http.StatusNotFound)
		log.Fatalln(err)
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.WriteHeader(http.StatusOK)
	fmt.Fprintln(w, string(res))
}
