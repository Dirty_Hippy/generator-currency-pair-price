package handlers

import (
	"errors"
	"generator-currency-pair-price/internals/app/processors"
	"github.com/gorilla/mux"
	"net/http"
)

type CurrencyPairHandler struct {
	processor *processors.CurrencyPairProcessor
}

func NewCurrencyPairHandler(processor *processors.CurrencyPairProcessor) *CurrencyPairHandler {
	handler := new(CurrencyPairHandler)
	handler.processor = processor
	return handler
}

// @Summary Show all prices for all currency pairs
// @Tags List
// @Description GetCurrencyPairList
// @ID GetCurrencyPairList
// @Accept  json
// @Produce  json
// @Success 200 {object} []models.CurrencyPair
// @Failure 400,404 {object} errorResponse
// @Failure 500 {object} errorResponse
// @Failure default {object} errorResponse
// @Router currency_pair/list [get]

func (handler *CurrencyPairHandler) GetCurrencyPairList(w http.ResponseWriter, r *http.Request) {
	list := handler.processor.GetCurrencyPairList()

	response := Response{Result: "OK", Data: list}
	WrapOK(w, response)
}

// @Summary Show prices for currency pair
// @Tags Find
// @Description GetPricesByCurrencyPair
// @ID GetPricesByCurrencyPair
// @Accept  json
// @Produce  json
// @Param id
// @Success 200 {object} models.CurrencyPair
// @Failure 400,404 {object} errorResponse
// @Failure 500 {object} errorResponse
// @Failure default {object} errorResponse
// @Router currency_pair/find{id} [get]

func (handler *CurrencyPairHandler) GetPricesByCurrencyPair(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	if vars["id"] == "" {
		WrapError(w, errors.New("missing id"))
		return
	}

	id := vars["id"]

	prices := handler.processor.GetPricesByCurrencyPair(id)
	response := Response{Result: "OK", Data: prices}

	WrapOK(w, response)
}
