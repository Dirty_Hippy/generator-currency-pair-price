package handlers

type Response struct {
	Result string      `json:"result"`
	Data   interface{} `json:"data"`
}
