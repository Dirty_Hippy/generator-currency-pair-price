package app

import (
	"generator-currency-pair-price/internals/app/models"
	"generator-currency-pair-price/internals/storage"
	"math/rand"
	"time"
)

func RunGenerator(storage *storage.CurrencyPairStorage, currencyPairs []string, cacheSize int, addSeconds int) {
	for {
		for i, pair := range currencyPairs {
			prices := make([]float32, cacheSize)
			for j := 0; j < cacheSize; j++ {
				t := time.Now().Add(time.Second * time.Duration(addSeconds))
				prices[j] = float32(t.Unix()) / float32(rand.Intn(cacheSize-1)+1)
			}
			storage.CurrencyPairs[i] = models.CurrencyPair{
				Name:   pair,
				Prices: prices,
			}
		}

		time.Sleep(time.Second * 10)
	}
}
