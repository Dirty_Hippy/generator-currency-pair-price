package models

type CurrencyPair struct {
	Name   string    `json:"currency_pair"`
	Prices []float32 `json:"prices"`
}
