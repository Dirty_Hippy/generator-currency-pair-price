package storage

import (
	"generator-currency-pair-price/internals/app/models"
)

type CurrencyPairStorageInterface interface {
	GetCurrencyPairList() []models.CurrencyPair
	GetPricesByCurrencyPair(currencyPair string) models.CurrencyPair
}

type CurrencyPairStorage struct {
	CurrencyPairs []models.CurrencyPair
}

func NewCurrencyPairStorage(size int) *CurrencyPairStorage {
	storage := new(CurrencyPairStorage)
	storage.CurrencyPairs = make([]models.CurrencyPair, size)
	return storage
}

func (storage *CurrencyPairStorage) GetCurrencyPairList() []models.CurrencyPair {
	return storage.CurrencyPairs
}

func (storage *CurrencyPairStorage) GetPricesByCurrencyPair(currencyPair string) models.CurrencyPair {
	var result models.CurrencyPair
	for _, pair := range storage.CurrencyPairs {
		if pair.Name == currencyPair {
			result = pair
			break
		}
	}
	return result
}
