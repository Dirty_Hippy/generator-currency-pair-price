package cfg

import (
	"github.com/spf13/viper"
)

type Cfg struct {
	Port          string `mapstructure:"PORT"`
	CurrencyPairs string `mapstructure:"CURRENCY_PAIRS"`
	AddSeconds    int    `mapstructure:"ADD_SECONDS"`
	CacheSize     int    `mapstructure:"CACHE_SIZE"`
}

func LoadConfig(path string) (config Cfg, err error) {
	viper.AddConfigPath(path)
	viper.SetConfigName("app")
	viper.SetConfigType("env")
	viper.AutomaticEnv()

	err = viper.ReadInConfig()
	if err != nil {
		return
	}

	err = viper.Unmarshal(&config)
	return
}

//TODO add config validation
