package main

import (
	"context"
	"generator-currency-pair-price/api"
	"generator-currency-pair-price/api/middleware"
	_ "generator-currency-pair-price/docs"
	"generator-currency-pair-price/internals/app"
	"generator-currency-pair-price/internals/app/handlers"
	"generator-currency-pair-price/internals/app/processors"
	"generator-currency-pair-price/internals/cfg"
	"generator-currency-pair-price/internals/storage"
	"github.com/sirupsen/logrus"
	"log"
	"os"
	"os/signal"
	"strings"
	"syscall"
)

// @title           Generator currency pair price
// @version         1.0
// @description     API Server for generation currency pair price
// @host      		localhost:8080
// @BasePath  		/

func main() {
	log.Println("Starting server")

	config, err := cfg.LoadConfig(".")
	if err != nil {
		log.Fatalln("cannot load config:", err)
	}

	currencyPairs := strings.Split(config.CurrencyPairs, " ")
	cacheSize := config.CacheSize
	addSeconds := config.AddSeconds

	currencyPairStorage := storage.NewCurrencyPairStorage(len(currencyPairs))
	currencyPairProcessor := processors.NewCurrencyPairProcessor(currencyPairStorage)
	currencyPairHandler := handlers.NewCurrencyPairHandler(currencyPairProcessor)

	routes := api.CreateRoutes(currencyPairHandler)
	routes.Use(middleware.RequestLog)

	server := app.NewServer(config)
	log.Println("Server started")

	go app.RunGenerator(currencyPairStorage, currencyPairs, cacheSize, addSeconds)

	go func() {
		quit := make(chan os.Signal, 1)
		signal.Notify(quit, syscall.SIGTERM, syscall.SIGINT)
		<-quit

		log.Println("Shutting Down")

		if err := server.Shutdown(context.Background()); err != nil {
			logrus.Errorf("error occured on server shutting down: %s", err.Error())
		}
	}()

	if err := server.Run(routes); err != nil {
		log.Fatalln(err)
	}
}
