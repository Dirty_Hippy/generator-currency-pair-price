run:
	go run cmd/main.go

build:
	go build cmd/main.go

lint:
	golangci-lint run

test:
	go test ./... -v

test-coverage:
	go test -cover ./...

docker-build:
	docker build -t generator .

docker-run:
	docker run --name=generator -p 80:8080 generator

swag:
	swag init -g cmd/main.go
